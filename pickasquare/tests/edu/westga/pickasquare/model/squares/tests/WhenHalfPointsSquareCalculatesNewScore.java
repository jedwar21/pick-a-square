package edu.westga.pickasquare.model.squares.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.squares.HalfPointsSquare;

/**
 * WhenDoublePointsSquareCalculatesNewScore defines test cases
 * for a DoublePointsSquare calculating a new score.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class WhenHalfPointsSquareCalculatesNewScore {

	private HalfPointsSquare pointsCutter;
	
	/**
	 * No-op constructor.
	 */
	public WhenHalfPointsSquareCalculatesNewScore() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		this.pointsCutter = new HalfPointsSquare();
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.pointsCutter.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void squaresScoreShouldHaveBeenCalculated() {
		this.pointsCutter.calculateNewScore(100);
		assertTrue(this.pointsCutter.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void shouldGet0WhenDoubleScoreOf0() {
		int newScore = this.pointsCutter.calculateNewScore(0);
		assertEquals(0, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void shouldGet200WhenDoubleScoreOf100() {
		int newScore = this.pointsCutter.calculateNewScore(100);
		assertEquals(50, newScore);
	}

}
