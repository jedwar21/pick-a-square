package edu.westga.pickasquare.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.westga.pickasquare.model.squares.GameSquare;

/**
 * GameBoard defines the board on which the players
 * make their moves.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public final class GameBoard {
	
	/**
	 * The number of GameSquares on a GameBoard.
	 */
	public static final int BOARD_SIZE = 16;

	private final List<GameSquare> squares;	
	
	/*
	 * Called by the Builder to create a new GameBoard instance.
	 */
	private GameBoard(Builder aBuilder) {
		if (aBuilder == null) {
			throw new IllegalArgumentException("Null Builder");
		}		
		
		this.squares = aBuilder.getSquares();
		
		if (aBuilder.shouldShuffle()) {
			Collections.shuffle(this.squares);
		}
	}
	
	/**
	 * Returns the number of squares on this board.
	 * 
	 * @precondition	none
	 * @return			how many GameSquares are on this board
	 */
	public int size() {
		return this.squares.size();
	}
	
	/**
	 * Returns the GameSquare object at the specified location.
	 * 
	 * @precondition	0 <= index < BOARD_SIZE
	 * @return			the square
	 * 
	 * @param index		the square's position on this board
	 */
	public GameSquare getSquare(int index) {
		if (index < 0) {
			throw new IllegalArgumentException("Negative index");
		}
		if (index >= BOARD_SIZE) {
			throw new IllegalArgumentException("Index out of range");
		}
		
		return this.squares.get(index);
	}
	
	//this is called a nested class. to call GameBoard.Builder()
	//when you need to add a lot of things to an object in the constructor
	/**
	 * Builder allows client objects to build a GameBoard instance
	 * by adding GameSquares to it.
	 * 
	 * @author CS 1302
	 * @version Spring, 2015
	 */
	public static class Builder {
		
		private final List<GameSquare> squares;
		private final boolean shuffle;
		
		/**
		 * Creates a new Builder instance with an empty collection
		 * of GameSquares that may or may not be shuffled, depending
		 * on the specified parameter value.
		 * <p>
		 * Parameter shuffle should be be set false for development,
		 * but should be true for production.
		 * 
		 * @precondition 	none
		 * @postcondition	size() == 0 
		 * 
		 * @param shuffle	true iff the squares on the board should
		 * 					be shuffled in random order
		 */
		public Builder(boolean shuffle) {
			this.shuffle = shuffle;
			this.squares = 
					new ArrayList<GameSquare>(GameBoard.BOARD_SIZE);
			//telling the size saves some time
		}
		
		/**
		 * Appends the specified element to the end of the list of squares.
		 * Returns this Builder object so that calls to this method can 
		 * be chained. 
		 * 
		 * @precondition	aSquare != null
		 * @postcondition	size() == size()@prev + 1
		 * @return	this Builder object
		 * 
		 * @param aSquare	the square to append
		 */
		public Builder addSquare(GameSquare aSquare) {
			if (aSquare == null) {
				throw new IllegalArgumentException("Null GameSquare");
			}
			
			this.squares.add(aSquare);
			return this;
			//return this = return the builder object
		}
		
		/**
		 * Returns the number of elements added to this Builder.
		 * 
		 * @precondition 	none
		 * @return			how many squares have been added so far
		 */
		public int size() {
			return this.squares.size();
		}
		
		/**
		 * Returns the GameBoard instance built by this builder.
		 * 
		 * @precondition	size() == GameBoard.BOARD_SIZE
		 * @return	the GameBoard
		 */
		public GameBoard getGameBoard() {
			if (this.size() != GameBoard.BOARD_SIZE) {
				throw new IllegalStateException("Not enough GameSquares");
			}
			
			return new GameBoard(this);
		}
		
		private List<GameSquare> getSquares() {
			return this.squares;
		}

		private boolean shouldShuffle() {
			return this.shuffle;
		}

	}
}