/**
 * 
 */
package edu.westga.pickasquare.model;

/**
 * @author Jessica
 *
 */
public abstract class AbstractPlayer implements Player {

	private int score;
	/**
	 * 
	 */
	public AbstractPlayer() {
		// TODO Auto-generated constructor stub
		this.score = 0;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.Player#setScore(int)
	 */
	@Override
	public abstract void setScore(int score);

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.Player#getScore()
	 */
	@Override
	public int getScore() {
		// TODO Auto-generated method stub
		return this.score;
	}

}
