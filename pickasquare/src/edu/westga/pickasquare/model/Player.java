package edu.westga.pickasquare.model;

/**
 * Player defines the interface for a person playing the pick-a-square game
 * following the rules of the game indicated by the concrete class.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 */
public interface Player {

	/**
	 * Sets the player's score.
	 * 
	 * @precondition score >= 0
	 * @postcondition getScore() == score
	 * 
	 * @param score
	 *            the score to set
	 */
	void setScore(int score);

	/**
	 * Returns the player's score.
	 * 
	 * @precondition none
	 * @return the score
	 */
	int getScore();

}