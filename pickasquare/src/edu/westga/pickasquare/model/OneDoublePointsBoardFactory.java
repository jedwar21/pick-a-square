package edu.westga.pickasquare.model;

import edu.westga.pickasquare.model.squares.AddPointsSquare;
import edu.westga.pickasquare.model.squares.DoublePointsSquare;
import edu.westga.pickasquare.model.squares.GoBustSquare;

//factories are when a all this stuff needs to be in a constructor method
/**
 * OneDoublePointsBoardFactory defines a factory that
 * uses a GameBoard.Builder to create a GameBoard with
 * these squares: 
 * <ul>
 * <li> 2 GoBustSquares</li>
 * <li> 1 DoublePointsSquares</li>
 * <li> 5 AddPointsSquares worth 100 points</li>
 * <li> 4 AddPointsSquares worth 200 points</li>
 * <li> 4 AddPointsSquares worth 400 points</li>
 * </ul>
 * 
 * The squares are in the order described unless shuffling
 * is specified, in which case they are in random order. 
 * 
 * @author CS 130
 * @version Spring, 015
 */
public class OneDoublePointsBoardFactory implements GameBoardFactory {
	
	private GameBoard.Builder theBuilder;

	/**
	 * Creates a new OneDoublePointsBoardFactory instance populated
	 * with GameBoard.BOARD_SIZE GameSquare objects whose classes
	 * are as described in the class comment.
	 * 
	 * @precondition	none
	 * @postcondition	getSquares().size == GameBoard.BOARD_SIZE, &&
	 * 					the squares are shuffled iff shouldShuffle is true
	 * 
	 * @param shouldShuffle true iff the squares on the board should
		 * 					be shuffled in random order
	 */
	public OneDoublePointsBoardFactory(boolean shouldShuffle) {
		this.theBuilder = new GameBoard.Builder(shouldShuffle);
		this.createSquares();
	}
	
	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameBoardFactory#getSquares()
	 */
	@Override
	public GameBoard getGameBoard() {
		return this.theBuilder.getGameBoard();
	}
	
	private void createSquares() {
		this.addTwoGoBustSquares();
		this.addOneDoubleScoreSquare();
		this.addFive100PointsSquares();
		this.addFour200PointSquares();
		this.addFour400PointSquares();	
	}

	private void addTwoGoBustSquares() {
		this.theBuilder.addSquare(new GoBustSquare())
					   .addSquare(new GoBustSquare());
	}

	private void addOneDoubleScoreSquare() {
		this.theBuilder.addSquare(new DoublePointsSquare());
	}

	private void addFive100PointsSquares() {
		//AVOID WHILE LOOPS!!!! whenever possible, use forcounting loops. 
		//Foreach least likely to screw up
		for (int i = 0; i < 5; i++) {
			this.theBuilder.addSquare(new AddPointsSquare(100));
		}
	}

	private void addFour200PointSquares() {
		for (int i = 0; i < 4; i++) {
			this.theBuilder.addSquare(new AddPointsSquare(200));
		}
	}

	private void addFour400PointSquares() {
		for (int i = 0; i < 4; i++) {
			this.theBuilder.addSquare(new AddPointsSquare(400));
		}
	}
}
