package edu.westga.pickasquare.model;

/**
 * RegularPlayer represents a person playing the pick-a-square game following
 * the normal rules of the game.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public final class RegularPlayer extends AbstractPlayer {

	private int score;

	/**
	 * Creates a new RegularPlayer instance.
	 * 
	 * @precondition none
	 * @postcondition getScore() == 0
	 */
	public RegularPlayer() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.westga.pickasquare.model.Player#setScore(int)
	 */
	@Override
	public void setScore(int score) {
		this.score = score;
	}

//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see edu.westga.pickasquare.model.Player#getScore()
//	 */
//	@Override
//	public int getScore() {
//		return this.score;
//	}

}