package edu.westga.pickasquare.model.squares;


/**
 * AddPointsSquare defines a GameSquare that adds
 * a specified number of points to the player's score
 * when selected.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class AddPointsSquare extends AbstractGameSquare {

	private int pointsToAdd;
	

	/**
	 * Creates a new AddPointsSquare instance with the
	 * specified number of points.
	 * 
	 * @precondition	pointsToAdd > 0
	 * @postcondition	getDescription().equals("" + pointsToAdd) &&
	 * 					!scoreHasBeenCalculated()
	 * 
	 * @param pointsToAdd	how many points to add to the player's score
	 */
	public AddPointsSquare(int pointsToAdd) {
		super();
		
		if (pointsToAdd <= 0) {
			throw new IllegalArgumentException("Points to add must be > 0");
		}
		
		this.pointsToAdd = pointsToAdd;
		
	}
	
	/** 
	 * Implements the method by adding the points to add
	 * to the specified score.
	 * 
	 * @precondition	oldScore >= 0
	 * @postcondition	scoreHasBeenCalculated()
	 * @return oldScore + the points to add
	 */
	@Override
	public int calculateNewScore(int oldScore) {
		super.calculateNewScore(oldScore);
		return this.pointsToAdd + oldScore;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameSquare#getDescription()
	 */
	@Override
	public String getDescription() {
		return "" + this.pointsToAdd;
	}

}