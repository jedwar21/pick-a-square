package edu.westga.pickasquare.model.squares;


/**
 * DoublePointsSquare defines a GameSquare that doubles
 * the player's score when selected.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class DoublePointsSquare extends AbstractGameSquare {
	
	/**
	 * Creates a new DoublePointsSquare instance.
	 * 
	 * @precondition	none
	 * @postcondition	!scoreHasBeenCalculated()
	 */
	public DoublePointsSquare() {
		super();
	}

	/**  
	 * Implements the method by doubled the specified score.
	 * 
	 * @precondition	oldScore >= 0
	 * @postcondition	scoreHasBeenCalculated()
	 * @return 2 * oldScore
	 */
	@Override
	public int calculateNewScore(int oldScore) {
		super.calculateNewScore(oldScore);
		return 2 * oldScore;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameSquare#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Double!";
	}

}
