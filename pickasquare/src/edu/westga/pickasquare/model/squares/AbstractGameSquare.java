package edu.westga.pickasquare.model.squares;


/**
 * AbstractGameSquare defines a base class for concrete
 * GameSquare implementations.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public abstract class AbstractGameSquare implements GameSquare {
	
	private boolean scorePreviouslyCalculated;
	
	/**
	 * Instantiates the data members shared by subclasses.
	 * 
	 * @precondition	none
	 * @postcondition	!scoreHasBeenCalculated()
	 */
	public AbstractGameSquare() {
		this.scorePreviouslyCalculated = false;
	}

	/**
	 * Partially implement the method: checks the precondition and
	 * marks the object as having had its score calculated, but
	 * just returns Integer.MAX_VALUE as the new score.
	 * <p>
	 * <strong>
	 * Must be overridden by concrete classes to calculate the new score.
	 * </strong>
	 * 
	 * @return Integer.MAX_VALUE
	 */
	@Override
	public int calculateNewScore(int oldScore) {
		if (oldScore < 0) {
			throw new IllegalArgumentException("Old score must be >= 0");
		}
		
		this.scorePreviouslyCalculated = true;
		return Integer.MAX_VALUE;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameSquare#getDescription()
	 */
	@Override
	public abstract String getDescription();

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameSquare#scoreHasBeenCalculated()
	 */
	@Override
	public boolean scoreHasBeenCalculated() {
		return this.scorePreviouslyCalculated;
	}

}
