package edu.westga.pickasquare.model;

/**
 * PrivilegedPlayer represents a person playing the pick-a-square game following
 * the "bonus points" rules of the game.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 */
public final class PrivilegedPlayer extends AbstractPlayer {

	private int score;
	private int bonusPoints;

	/**
	 * Creates a new PrivilegedPlayer instance.
	 * 
	 * @precondition bonusPoints > 0
	 * @postcondition getScore() == 0
	 * 
	 * @param bonusPoints
	 *            the extra points added to the score of a privileged player
	 */
	public PrivilegedPlayer(int bonusPoints) {
		super();
		
		if (bonusPoints < 1) {
			throw new IllegalArgumentException("Bonus points must have value");
		}

		this.bonusPoints = bonusPoints;
	}

	/**
	 * Sets the player's score.
	 * 
	 * @precondition score >= 0
	 * @postcondition getScore() == score
	 * 
	 * @param score
	 *            the score to set
	 */
	public void setScore(int score) {
		if (score < 0) {
			throw new IllegalArgumentException("Score cannot be negative");
		}

		this.score = score + this.bonusPoints;
	}

//	/**
//	 * Returns the player's score.
//	 * 
//	 * @precondition none
//	 * @return the score
//	 */
//	public int getScore() {
//		return this.score;
//	}

}
