package edu.westga.pickasquare;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

/**
 * PickASquareApplication extends the JavaFX Application class
 * to build the GUI and start program execution.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class PickASquareApplication extends Application {
	
	private static final String WINDOW_TITLE = "Pick a square!";
	private static final String GUI_FXML = "view/Gui.fxml";

	/**
	 * Constructs a new Application object for the login demo
	 * program.
	 * 
	 * @precondition	none
	 * @postcondition	the object is ready to execute
	 */
	public PickASquareApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the edu.westga.playorhold.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}