package edu.westga.pickasquare.view;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import edu.westga.pickasquare.controllers.GameController;
import edu.westga.pickasquare.model.GameBoard;
import edu.westga.pickasquare.model.GameBoardFactory;
import edu.westga.pickasquare.model.OneHalfPointsBoardFactory;
import edu.westga.pickasquare.model.Player;
import edu.westga.pickasquare.model.PrivilegedPlayer;
import edu.westga.pickasquare.model.RegularPlayer;

/**
 * ViewModel defines the view model for the play-or-hold application.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public final class ViewModel {

	private String descriptionOfSelectedSquare;

	private final Player player1;
	private final PrivilegedPlayer player2;
	private final GameBoard theBoard;

	private final GameController theGameController;

	private StringProperty player1Score;
	private StringProperty player2Score;
	private StringProperty gameStatusLabel;

	/**
	 * Creates a new ViewModel instance.
	 * 
	 * @precondition iff PrivilegedPlayer is playing,
	 *               bonusPointsForPrivilegedPlayer > 0
	 * @postcondition the object and its properties exist
	 * 
	 */
	public ViewModel() {

		this.descriptionOfSelectedSquare = "";

		this.player1 = new RegularPlayer();
		this.player2 = new PrivilegedPlayer(100);

		GameBoardFactory factory = new OneHalfPointsBoardFactory(true);
		this.theBoard = factory.getGameBoard();

		this.theGameController = new GameController(this.theBoard,
				this.player1, this.player2);

		this.player1Score = new SimpleStringProperty("0");
		this.player2Score = new SimpleStringProperty("0");
		this.gameStatusLabel = new SimpleStringProperty("Player 1's turn");
	}

	/**
	 * Returns the property that represents the score for player 1.
	 * 
	 * @precondition none
	 * @return the property
	 */
	public StringProperty player1ScoreProperty() {
		return this.player1Score;
	}

	/**
	 * Returns the property that represents the score for player 2.
	 * 
	 * @precondition none
	 * @return the property
	 */
	public StringProperty player2ScoreProperty() {
		return this.player2Score;
	}

	/**
	 * Tells the game controller to carry out the current player's move.
	 * 
	 * @precondition 1 <= gameSquareId <= 16
	 * @postcondition the current player's score reflects this move, &&
	 *                currentMoveValue() returns the value of the
	 * 
	 * @param gameSquareId
	 *            the ID of the selected control in the GUI
	 */
	public void play(String gameSquareId) {
		int squareIndex = Integer.parseInt(gameSquareId) - 1;
		this.theGameController.play(squareIndex);
		this.descriptionOfSelectedSquare = this.theBoard.getSquare(squareIndex)
				.getDescription();
		this.player1Score.setValue("" + this.player1.getScore());
		this.player2Score.setValue("" + this.player2.getScore());
		this.gameStatusLabel.setValue(
						this.theGameController.getCurrentPlayer().toString());
	}

	/**
	 * Returns the value of the current "move".
	 * 
	 * @precondition none
	 * @return the value of the selected game board item
	 */
	public String selectedSquareDescription() {
		return this.descriptionOfSelectedSquare;
	}

	/**
	 * Displays the current players turn.
	 * 
	 * @precondition none
	 * @postcondition the player is identified
	 * 
	 * @return the name of the player
	 */
	public Property<String> gameStatusLabelProperty() {
		
		return this.gameStatusLabel;
	}

}
